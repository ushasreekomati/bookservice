package com.epam.bookservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookException;
import com.epam.bookservice.repository.BookRepository;
import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.Assertions.assertThatCode;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {
	@InjectMocks
	private BookService bookService;

	@Mock
	private BookRepository bookRepository;

	private Book book;
	private List<Book> bookList;
	private Optional<Book> bookOp;
	private Book book2;

	@BeforeEach
	public void setup() {
		book = new Book(1L, "title", "genre", "author");
		bookList = new ArrayList<>();
		bookOp = Optional.of(book);
		book2 = new Book();
		book2.setId(2L);
		book2.setTitle("title2");
		book2.setGenre("Genre2");
		book2.setAuthor("Author2");
		bookList.add(book);
		bookList.add(book2);
	}

	@Test
	void testGetBooks() {

		when(bookRepository.findAll()).thenReturn(bookList);
		List<Book> result = bookService.getBooks();
		assertThat(result.size()).isEqualTo(2);
		assertThat(result.get(0).getAuthor()).isEqualTo(book.getAuthor());
		assertThat(result.get(0).getGenre()).isEqualTo(book.getGenre());
		assertThat(result.get(0).getTitle()).isEqualTo(book.getTitle());

		assertThat(result.get(1).toString()).isEqualTo(book2.toString());

	}

	@Test
	void testAddNewBook() {

		Optional<Book> book1 = bookRepository.findById(1L);
		when(bookRepository.save(book)).thenReturn(book);
		assertEquals(bookService.addNew(book), book);

	}

	@Test
	void testDelete() {

		bookRepository.save(book2);
		Optional<Book> book1 = Optional.of(book2);
		when(bookRepository.findById(2L)).thenReturn(book1);
		bookService.delete(2L);

	}

	@Test
	void testGetBookById() {

		bookRepository.save(book);
		when(bookRepository.findById(1L)).thenReturn(bookOp);
		Book result = bookService.getBookById(1L);
		assertThat(result.getAuthor()).isEqualTo(book.getAuthor());
		assertThat(result.getTitle()).isEqualTo(book.getTitle());

	}

	@Test
	void testUpdate() throws BookException {
		bookRepository.save(book);
		when(bookRepository.findById(1L)).thenReturn(bookOp);
		Book result1 = bookService.getBookById(1L);
		assertEquals(bookService.update(1L, book), bookOp);
	}

	@Test
	void givenNoBook_whenFindById_thenReturnError() {
		given(this.bookRepository.findById(1L)).willReturn(Optional.empty());
		assertThatCode(() -> bookService.getBookById(1234L)).isInstanceOf(BookException.class)
				.hasMessageContaining("Book not found: 1234L");
	}

	@Test
	void givenNoBook_whenUpdate_thenReturnError() {
		given(this.bookRepository.findById(1L)).willReturn(Optional.empty());
		assertThatCode(() -> bookService.update(1234L, new Book())).isInstanceOf(BookException.class)
				.hasMessageContaining("Book not found: 1234L");
	}

}
