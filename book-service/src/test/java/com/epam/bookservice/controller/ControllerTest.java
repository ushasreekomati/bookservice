package com.epam.bookservice.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest
class ControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	BookController bookController;
	@MockBean
	BookService bookService;

	List<Book> bookList;
	Book bookToBeAdded;
	Book bookAdded;
	Book book;

	@BeforeEach
	public void setup() {
		bookList = new ArrayList<>();
		bookList.add(new Book(1L, "title", "genre", "author"));
		bookList.add(new Book(2L, "title2", "genre2", "author2"));

		book = new Book(1L, "title", "genre", "author");

	}

	@Test
	public void testGetBooks() throws Exception {
		when(bookService.getBooks()).thenReturn(bookList);
		this.mockMvc.perform(get("/books")).andExpect(status().isOk());
	}

	@Test
	public void testGetBookById() throws Exception {
		when(bookService.getBookById(1L)).thenReturn(book);
		this.mockMvc.perform(get("/books").param("bookId", "1")).andExpect(status().isOk());
	}

	@Test
	public void testDeleteBook() throws Exception {
		doNothing().when(bookService).delete(1L);

		this.mockMvc.perform(delete("/books/1")).andExpect(status().isNoContent());
	}

	@Test
	public void testAddBook() throws Exception {
		
		Book book1= new Book();
		book1.setGenre("Genre 1");
		book1.setAuthor("Author 1");
		when(bookService.addNew(ArgumentMatchers.any())).thenReturn(book1);
		mockMvc.perform(post("/books").contentType("application/json")
				.content(asJsonString(book1))).andExpect(status().isCreated());
	}

	@Test
	public void testUpdateBook() throws Exception {
		when(bookService.update(1L, book)).thenReturn(book);
		this.mockMvc.perform(put("/books/1").contentType("application/json").content(asJsonString(book)))
				.andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}