package com.epam.bookservice.controller;

import java.net.URI;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookException;
import com.epam.bookservice.service.BookService;

@RequestMapping("/books")
@RestController
public class BookController {

	@Autowired
	BookService bookService;

	
    private static final Logger logger = LogManager.getLogger(BookController.class);

	@GetMapping
	public ResponseEntity<List<Book>> getBooks() {
		logger.info("Fetching books");
		return ResponseEntity.ok(bookService.getBooks());
	}

	@PostMapping
	public ResponseEntity<Book> addNew(@RequestBody Book book) {
		Book savedBook = bookService.addNew(book);
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/id").buildAndExpand(savedBook.getId())
				.toUri();
		return ResponseEntity.created(uri).body(savedBook);
	}

	@PutMapping("{bookId}")
	public ResponseEntity<Book> update(@PathVariable Long bookId, @RequestBody Book book) throws BookException {
		return ResponseEntity.ok(bookService.update(bookId, book));
	}

	@GetMapping("{bookId}")
	public ResponseEntity<Book> get(@PathVariable Long bookId) {
		return ResponseEntity.ok(bookService.getBookById(bookId));
	}

	@DeleteMapping("{bookId}")
	public ResponseEntity<?> delete(@PathVariable Long bookId) {
		bookService.delete(bookId);
		return ResponseEntity.noContent().build();
	}
}
