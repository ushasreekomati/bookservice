package com.epam.bookservice.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.bookservice.entity.Book;
import com.epam.bookservice.exception.BookException;
import com.epam.bookservice.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public List<Book> getBooks() {
		return bookRepository.findAll();
	}

	public Book getBookById(Long bookId) {
		return bookRepository.findById(bookId).map(book -> book)
				.orElseThrow(() -> new BookException("Book Not Found :" + bookId));
	}

	public Book addNew(Book book) {
		return bookRepository.save(book);

	}

	public Book update(Long bookId, Book book) throws BookException {
		if (bookId != book.getId())
			throw new BookException("BadRequest :" + bookId);

		return bookRepository.findById(bookId).map(b -> bookRepository.save(book))
				.orElseThrow(() -> new BookException("Book Not Found :" + bookId));
	}

	public void delete(Long bookId) {
		bookRepository.deleteById(bookId);

	}

}
