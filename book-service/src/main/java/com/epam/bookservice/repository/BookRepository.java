package com.epam.bookservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.epam.bookservice.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	
	void deleteById(Long id);

}
