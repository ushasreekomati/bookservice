package com.epam.bookservice.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class BookException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BookException(String message) {
		super(message);
	}

}
